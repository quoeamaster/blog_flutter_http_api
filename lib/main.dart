import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:http_api_demo_app/esQueryBuilder.dart';

void main() {
  runApp(MyApp());
}

/// A class to load the config file bundled with the App.
class _ConfigLoader {
  /// The config file's name (check your pubspec.yaml on the assets section).
  final String configFilename;
  /// The Map of configs converted from the config's data.
  Map<String, dynamic> _configMap;

  /// Constructor
  _ConfigLoader({this.configFilename});

  /// Retrieve the config value associated by the [key]. 
  /// 
  /// The [bundle] is required for the very first time to access config values. 
  /// Further access would simply ignore the [bundle].
  Future<String> getConfigByKey(AssetBundle bundle, String key) async {
    if (_configMap == null) {
      // read config file for the 1st time
      final String _configFromAsset = await bundle.loadString(configFilename);
      _configMap = json.decode(_configFromAsset);
    }
    
    if (_configMap != null) {
      return _configMap[key];
    }
    return null;
  }
}


class MyApp extends StatelessWidget {

  final _ConfigLoader _cL = _ConfigLoader(configFilename: "assets/config.json");

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: Text("demo on http package v0.1.0"),
            bottom: TabBar(
              tabs: [
                Tab(icon: Icon(Icons.cloud), child: Text("get weather"), ),
                Tab(icon: Icon(Icons.cloud_download), child: Text("get ES doc"), ),
                Tab(icon: Icon(Icons.cloud_upload), child: Text("add ES doc"), ),
              ],
            ),
          ),

          body: TabBarView(
            children: [
              _FeelBoredSuggestor(),
              _ESSearch(configLoader: _cL),
              _ESAddRecord(configLoader: _cL),
            ],
          ),

        ),
      ),
    );
  }
}

class _FeelBoredSuggestor extends StatefulWidget {
  State createState() => _FeelBoredSuggestorState();
}
class _FeelBoredSuggestorState extends State<_FeelBoredSuggestor> {
  /// The api's url to get "bored suggestion".
  static final String url = "https://www.boredapi.com/api/activity";

  /// The suggestion entity returned through the api.
  _EBoredSuggestion _suggestion;

  /// The controller for the result text-field. Displays bored suggestion to user.
  final TextEditingController _ctrl = TextEditingController();


  /// Building the main UI / layout.
  Widget build(BuildContext c) {

    return Padding(
      padding: const EdgeInsets.fromLTRB(12, 18, 12, 20),
      child: Column(
        children: [
          InkWell(
            onTap: () {
              _getSuggestion(c);
            },
            child: Text(
              "Feel bored? Click and Ask for a suggestion~", 
              style: TextStyle(fontSize: 16),
            ),
          ),

          Padding(
            padding: const EdgeInsets.fromLTRB(0, 16, 0, 0),
            child: TextField(
              style: Theme.of(c).textTheme.caption.copyWith(fontSize: 18),
              decoration: InputDecoration(
                hintText: "suggestion will be available here :)",
              ),
              controller: _ctrl,
              enabled: false,
              minLines: 10,
              maxLines: 100,
            )
          ),
          
        ],
      ),
    );
  }

  /// To retrieve the suggeston through the [url] api.
  /// 
  /// Contents returned would be a json string.
  Future<void> _getSuggestion(BuildContext c) async {
    http.Response _r = await http.get(url);
    String _json = _r.body;

    _suggestion = _EBoredSuggestion.fromJson(json.decode(_json));
    setState(() {
      _ctrl.text = """suggested activity for YOU~ 

- ${_suggestion.activity} 
- type [${_suggestion.type}]
- how many participants required? ${_suggestion.minParticipants}
- link: ${_suggestion.link??_suggestion.link}
""";
    });
  }

}
/// The entity class representing a Bored-suggestion. 
/// Use this class to unfold / decode from the json string returned throught he api.
class _EBoredSuggestion {
  final String activity;
  final String type;
  final String link;
  final int minParticipants;

  _EBoredSuggestion({this.activity, this.type, this.link, this.minParticipants});

  factory _EBoredSuggestion.fromJson(Map<String, dynamic> data) {
    return _EBoredSuggestion(
      activity: data["activity"],
      type: data["type"],
      link: data["link"],
      minParticipants: data["participants"],
    );
  }
}


/// The UI / Layout providing ES search features.
class _ESSearch extends StatefulWidget {
  /// The Config loader for accessing config values by key(s).
  final _ConfigLoader configLoader;

  /// Constructor.
  _ESSearch({this.configLoader});
  
  State createState() => _ESSearchState();
}
class _ESSearchState extends State<_ESSearch> {
  final TextEditingController _queryCtrl = TextEditingController();
  final TextEditingController _resultCtrl = TextEditingController();
  final TextEditingController _customerFirstNameCtrl = TextEditingController();

  /// Day-of-week chosen, default is -1 -> a Non-valid value.
  int _dayOfWeek = -1; 

  @override
  dispose() {
    _customerFirstNameCtrl.text = "";
    _queryCtrl.text = "";
    _resultCtrl.text = "";
    _dayOfWeek = -1;
    super.dispose();
  }

  Widget build(BuildContext c) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(12, 18, 12, 12),
          child: Text(
            "build a query:", 
            style: Theme.of(c).textTheme.headline6.copyWith(fontSize: 16)
          ),
        ),

        Padding(
          padding: const EdgeInsets.fromLTRB(8, 8, 4, 0),
          child: Column(
            children: [
              // day-of-week option
              Row(
                children: [
                  Text("day of week:", style: Theme.of(c).textTheme.caption.copyWith(fontSize: 16),),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(20, 0, 0, 20),
                      child: DropdownButton(
                        isExpanded: true,
                        value: _dayOfWeek,
                        onChanged: (_dayOfweek) {
                          _onDayOfWeekChange(_dayOfweek);
                        },
                        items: [ 
                          DropdownMenuItem<int>(
                            value: -1,
                            child: Text("[optional] choose a week-day")
                          ),
                          DropdownMenuItem<int>(
                            value: 0,
                            child: Text("Sunday")
                          ),
                          DropdownMenuItem<int>(
                            value: 1,
                            child: Text("Monday")
                          ), 
                          DropdownMenuItem<int>(
                            value: 2,
                            child: Text("Tuesday")
                          ),
                          DropdownMenuItem<int>(
                            value: 3,
                            child: Text("Wednesday")
                          ),
                          DropdownMenuItem<int>(
                            value: 4,
                            child: Text("Thursday")
                          ),
                          DropdownMenuItem<int>(
                            value: 5,
                            child: Text("Friday")
                          ),
                          DropdownMenuItem<int>(
                            value: 6,
                            child: Text("Saturday")
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              // 
              Row(
                children: [
                  Text("customer firstname:", style: Theme.of(c).textTheme.caption.copyWith(fontSize: 16),),
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(8, 0, 0, 8),
                      child: TextField(
                        controller: _customerFirstNameCtrl,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(8, 0, 0, 0),
                    child: Container(
                      width: 60,
                      child: RaisedButton(
                        onPressed: () => _runESQuery(c, -1),
                        child: Icon(Icons.search),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),

        // query body preview
        Container(
          height: 200,
          child: TextField(
            readOnly: true,
            controller: _queryCtrl,
            minLines: 10,
            maxLines: 1000,
          ),
        ),

        Padding(
          padding: const EdgeInsets.fromLTRB(12, 18, 12, 12),
          child: Text(
            "Results of the query:", 
            style: Theme.of(c).textTheme.headline6.copyWith(fontSize: 16)
          ),
        ),

        Container(
          height: 200,
          child: TextField(
            controller: _resultCtrl,
            readOnly: true,
            minLines: 10,
            maxLines: 1000,
          ),
        ),

      ],
    );
  }

  /// Set the day-of-week value for query building.
  _onDayOfWeekChange(int dayOfWeek) {
    setState(() {
      _dayOfWeek = dayOfWeek;
    });
  }

  /// Build the query based on the [queryType] and run against a ES cloud cluster.
  Future<void> _runESQuery(BuildContext c, int queryType) async {
    BoolQueryBuilder _b = BoolQueryBuilder();

    switch (queryType) {
      case -1:
        // build the query by builder
        // day-of-week valid
        if (_dayOfWeek != -1) {
          _b.addFilter(MatchQueryBuilder().field("day_of_week_i", _dayOfWeek));
        }
        // customer firstname valid?
        if (_customerFirstNameCtrl.text != null && _customerFirstNameCtrl.text.isNotEmpty) {
          _b.addMust(MatchQueryBuilder().field("customer_first_name", _customerFirstNameCtrl.text));
        }
        _queryCtrl.text = _prettyPrintJson(_b.build());
        break;
    }

    http.StreamedResponse _r = await _runQueryByClient(_b);
    String _json = await _r.stream.bytesToString();

    // update the controller with the prettified json
    _resultCtrl.text = _prettyPrintJson(_json);
    
    // update on UI
    setState(() {});
  }

  /// Convert the given [source] into a pretty-printable json format.
  String _prettyPrintJson(String source) {
    JsonEncoder _jEncoder = JsonEncoder.withIndent("  ");
    JsonDecoder _jDecoder = JsonDecoder();

    return _jEncoder.convert(_jDecoder.convert(source));
  }

  /// Execute the query to an ES cloud cluster and returns the StreamedResponse of the contents.
  Future<http.StreamedResponse> _runQueryByClient(BoolQueryBuilder builder) async {
    final http.Client _client = http.Client();
    final Map<String, String> _headers = Map<String, String>();
    http.Request _req;

    String esUrl = await widget.configLoader.getConfigByKey(rootBundle, "es_url");
    String esUsername = await widget.configLoader.getConfigByKey(rootBundle, "es_user");
    String esPwd = await widget.configLoader.getConfigByKey(rootBundle, "es_pwd");

    _req = http.Request("get", Uri.parse( "$esUrl/kibana_sample_data_ecommerce/_search" ));

    // prepare headers including contentType and Authorization token
    _headers["Content-Type"] = "application/json";
    _headers["Authorization"] = "Basic "+base64.encode(utf8.encode("$esUsername:$esPwd"));

    // set the body contents (in this case the ES queryDSL syntax)
    _req.body = builder.build();
    _req.headers.addAll(_headers);

    return _client.send(_req);
  }

}


/// The UI / Layout providing ES Add document feature.
class _ESAddRecord extends StatefulWidget {
  /// The Config loader for accessing config values by key(s).
  final _ConfigLoader configLoader;

  /// Constructor.
  _ESAddRecord({this.configLoader});
  
  State createState() => _ESAddRecordState();
}
class _ESAddRecordState extends State<_ESAddRecord> {

  final TextEditingController _sourceCtrl = TextEditingController();
  final TextEditingController _resultCtrl = TextEditingController();

  Widget build(BuildContext c) {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(16, 18, 16, 20),
          child: Text("Pick a sample json object to add to ES cluster:", 
            style: Theme.of(c).textTheme.headline6.copyWith(fontSize: 16),
          ),
        ),

        Padding(
          padding: const EdgeInsets.all(8.0),
          child: Row(
            mainAxisSize: MainAxisSize.max,
            children: [
              Expanded(
                child: RaisedButton(
                  padding: const EdgeInsets.fromLTRB(0, 12, 0, 12),
                  onPressed: () {
                    _add(c, 0);
                  },
                  child: Column(
                    children: [
                      Icon(Icons.directions_run),
                      Text(""),
                      Text("jogging activity")
                    ],
                  ),
                ),
              ),
              SizedBox(width: 3,),
              Expanded(
                child: RaisedButton(
                  padding: const EdgeInsets.fromLTRB(0, 12, 0, 12),
                  onPressed: () {
                    _add(c, 1);
                  },
                  child: Column(
                    children: [
                      Icon(Icons.collections_bookmark),
                      Text(""),
                      Text("reading activity")
                    ],
                  ),
                ),
              ),

            ],
          ),
        ),

        Padding(
          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: TextField(
            controller: _sourceCtrl,
            decoration: InputDecoration(
              hintText: "the chosen json document's content..."
            ),
            readOnly: true,
            minLines: 10,
            maxLines: 1000,
          ),
        ),

        Padding(
          padding: const EdgeInsets.fromLTRB(16, 18, 16, 20),
          child: Text("Results:", 
            style: Theme.of(c).textTheme.headline6.copyWith(fontSize: 16),
          ),
        ),

        Padding(
          padding: const EdgeInsets.fromLTRB(8, 0, 8, 0),
          child: Container(
            height: 200,
            child: TextField(
              controller: _resultCtrl,
              decoration: InputDecoration(
                hintText: "results in json"
              ),
              readOnly: true,
              minLines: 5,
              maxLines: 1000,
            ),
          ),
        ),

      ],
    );
  }

  Future<void> _add(BuildContext c, int docPicked) async {
    switch (docPicked) {
      case 0:
        _sourceCtrl.text = _docJog;
        break;
      case 1:
        _sourceCtrl.text = _docRead;
        break;
    }
    // run esPost
    await _runESPost();

    // force UI to update
    setState(() {});
  }

  Future<void> _runESPost() async {
    String esUrl = await widget.configLoader.getConfigByKey(rootBundle, "es_url");
    String esIndex = await widget.configLoader.getConfigByKey(rootBundle, "es_index");
    String esUsername = await widget.configLoader.getConfigByKey(rootBundle, "es_user");
    String esPwd = await widget.configLoader.getConfigByKey(rootBundle, "es_pwd");

    http.Response _r = await http.post("$esUrl/$esIndex/_doc", 
      headers: {
        HttpHeaders.contentTypeHeader: "application/json",
        HttpHeaders.authorizationHeader: "Basic "+base64.encode(utf8.encode("$esUsername:$esPwd"))
      }, 
      body: _sourceCtrl.text
    );

    JsonDecoder _jD = JsonDecoder();
    JsonEncoder _jE = JsonEncoder.withIndent("  ");
    _resultCtrl.text = _jE.convert( _jD.convert(_r.body) );
  }

  static final String _docJog = """
  {
    "activity": "jog",
    "type": "outdoors",
    "min_participant": 1
  }""";
  static final String _docRead = """
  {
    "activity": "read",
    "type": "indoors",
    "min_participant": 1
  }""";

}
