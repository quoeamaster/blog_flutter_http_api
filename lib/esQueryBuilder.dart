
/// The Abstract Builder containing all core features for building the QueryDSL syntax.
abstract class QueryBuilder {
  /// List of fields to be included for the response.
  List<String> __source = [];

  /// Set the fields to be included for the response.
  QueryBuilder sourceInclude(List<String> s) {
    //__source.addAll(s);
    if (s != null) {
      __source = s;
    } else {
      __source = [];
    }
    return this;
  }

  /// Build the whole QueryDSL syntax including the _source, query, aggs etc.
  String build();

  /// Build the QueryDSL syntax based on the attributes / properties of the builder instance.
  String buildQueryPartsOnly();

  /// Build the "_source" queryDSL syntax based on the settings from [sourceInclude] method earlier.
  String buildSourcePartsOnly() {
    // translate list of dynamic to queryDSL list format
    String _v = "";
    int _i = 0;

    __source.forEach((_field) { 
      if (_i > 0) {
        _v += ", ";
      }
      _v += '"$_field"';
      _i++;
    });
    if (_i > 0) {
      _v = "[ $_v ]";
    } else {
      _v = '"*"';
    }

    return '"_source": { "includes": $_v }';
  }

  /// Resets the conditions of the builder. Override this method to implement specific resets.
  void reset() {
    __source = [];
  }
  
  /// Translates the Operator's value to the corresponding operator's String value for the query.
  String translateOperatorEnumToString(Operator ops) {
    if (ops == Operator.or) {
      return "or";
    } else {
      return "and";
    }
  }

  /// Translates the Range-Operator's value to the corresponding operator's String value for the query.
  String translateRangeOperatorEnumToString(RangeOperator ops) {
    switch (ops) {
      case RangeOperator.gt:
        return "gt";
        break;
      case RangeOperator.gte:
        return "gte";
        break;
      case RangeOperator.lt:
        return "lt";
        break;
      case RangeOperator.lte:
        return "lte";
        break;
      default:
        print("non handled range-ops -> $ops");
        return "gte";
    }
  }

  /// Add / apply quotes to the given value. Quotes would be required if it is a String.
  String applyQuoteToValue(dynamic value) {
    String _t = value.runtimeType.toString();
    switch (_t) {
      case "String":
        return '"$value"';
        break;
      default:
        print("non String or un-handled case -> $_t");
        return "$value";
    }
  }

}

/// Operator for the query - OR (default), AND.
enum Operator {
  or,
  and,
}

/// Range operator for the range query - gt, gte, lt, lte.
enum RangeOperator {
  gt,
  gte,
  lt,
  lte
}

/// The Builder for Match-Query.
class MatchQueryBuilder extends QueryBuilder {
  /// The name of the target field for query.
  String _field;
  dynamic _value;
  Operator _operator;

  MatchQueryBuilder field(String field, dynamic value, { Operator operator = Operator.or }) {
    _field = field;
    _value = value;
    _operator = operator;

    return this;
  }

  @override
  reset() {
    super.reset();
    _field = null;
    _value = null;
    _operator = Operator.or;
  }

  /// Build the match Query syntax.
  @override
  String buildQueryPartsOnly() {
    return '{ "match": { "$_field": { "query": ${applyQuoteToValue(_value)}, "operator": "${translateOperatorEnumToString(_operator)}" } } }';
  }

  @override
  String build() {
    StringBuffer _s = StringBuffer();
    String _v;

    _s.write('{ ');

    // _source available?
    if (__source != null) {
      _s.write(buildSourcePartsOnly());
      _s.write(", ");
    }

    // has field set?
    if (_field != null) {
      _s.write('"query": ');
      _s.write(buildQueryPartsOnly());
      _s.write(", ");
    }

    // remove the last ", " if available
    if (_s.toString().endsWith(", ")) {
      _v = _s.toString();
      _v = _v.substring(0, _v.length - 2) + " }";
    } else {
      _s.write(' }');
      _v = _s.toString();
    }
    return _v;
  }
}

/// The Builder for Range-Query.
class RangeQueryBuilder extends QueryBuilder {
  
  String _field;
  num _value1;
  RangeOperator _ops1;
  num _value2;
  RangeOperator _ops2;

  /// Set the [field] with the given [value]. 
  /// 
  /// The parameter [ops] is optional and default value is [RangeOperator.gte].
  RangeQueryBuilder field(String field, num value, { RangeOperator ops = RangeOperator.gte, num value2, RangeOperator ops2 = RangeOperator.gte } ) {
    _field = field;
    _value1 = value;
    _ops1 = ops;

    _value2 = value2;
    _ops2 = ops2;

    return this;
  }

  @override
  reset() {
    super.reset();
    _field = null;
    _value1 = null;
    _ops1 = RangeOperator.gte;

    _value2 = null;
    _ops2 = RangeOperator.gte;
  }

  /// Build the range Query syntax.
  @override
  String buildQueryPartsOnly() {
    if (_value1 != null && _value2 != null) {
      return '{ "range": { "$_field": { ${applyQuoteToValue( translateRangeOperatorEnumToString(_ops1) )}: $_value1, ${applyQuoteToValue( translateRangeOperatorEnumToString(_ops2) )}: $_value2 } } }';
    } else {
      // assume only _value1 is provided
      return '{ "range": { "$_field": { ${applyQuoteToValue( translateRangeOperatorEnumToString(_ops1) )}: $_value1 } } }';
    }
  }

  @override
  String build() {
    StringBuffer _s = StringBuffer();
    String _v;

    _s.write('{ ');

    // _source available?
    if (__source != null) {
      _s.write(buildSourcePartsOnly());
      _s.write(", ");
    }

    // has field set?
    if (_field != null) {
      _s.write('"query": ');
      _s.write(buildQueryPartsOnly());
      _s.write(", ");
    }

    // remove the last ", " if available
    if (_s.toString().endsWith(", ")) {
      _v = _s.toString();
      _v = _v.substring(0, _v.length - 2) + " }";
    } else {
      _s.write(' }');
      _v = _s.toString();
    }
    return _v;
  }
}

/// The Builder for Bool-Query.
class BoolQueryBuilder extends QueryBuilder {

  List<QueryBuilder> _musts = [];
  List<QueryBuilder> _mustNots = [];
  List<QueryBuilder> _shoulds = [];
  List<QueryBuilder> _filters = [];

  /// Add a QueryBuilder instance (e.g. [MatchQueryBuilder] or [RangeQueryBuilder]) to the "must" clause of bool query.
  BoolQueryBuilder addMust(QueryBuilder builder) {
    if (builder != null) {
      _musts.add(builder);
    }
    return this;
  }

  /// Add a QueryBuilder instance (e.g. [MatchQueryBuilder] or [RangeQueryBuilder]) to the "must_not" clause of bool query.
  BoolQueryBuilder addMustNot(QueryBuilder builder) {
    if (builder != null) {
      _mustNots.add(builder);
    }
    return this;
  }

  /// Add a QueryBuilder instance (e.g. [MatchQueryBuilder] or [RangeQueryBuilder]) to the "should" clause of bool query.
  BoolQueryBuilder addShould(QueryBuilder builder) {
    if (builder != null) {
      _shoulds.add(builder);
    }
    return this;
  }

  /// Add a QueryBuilder instance (e.g. [MatchQueryBuilder] or [RangeQueryBuilder]) to the "filter" clause of bool query.
  BoolQueryBuilder addFilter(QueryBuilder builder) {
    if (builder != null) {
      _filters.add(builder);
    }
    return this;
  }


  @override
  reset() {
    super.reset();
    _musts = [];
    _mustNots = [];
    _shoulds = [];
    _filters = [];
  }

  /// Build the bool Query syntax.
  @override
  String buildQueryPartsOnly() {
    StringBuffer _s = StringBuffer();
    bool _anyClauseAdded = false;

    _s.write('{ "bool": { ');

    // must available?
    if (_musts.length > 0) {
      _anyClauseAdded = true;

      _s.write(' "must": [ ');
      _s.write(_buildQueryBuildersString(_musts));
      _s.write(' ]');
    }

    // must_not available?
    if (_mustNots.length > 0) {
      // append ", " ?
      if (_anyClauseAdded) {
        _s.write(", ");
      }
      _anyClauseAdded = true;

      _s.write(' "must_not": [ ');
      _s.write(_buildQueryBuildersString(_mustNots));
      _s.write(' ]');
    }

    // should available?
    if (_shoulds.length > 0) {
      // append ", " ?
      if (_anyClauseAdded) {
        _s.write(", ");
      }
      _anyClauseAdded = true;

      _s.write(' "should": [ ');
      _s.write(_buildQueryBuildersString(_shoulds));
      _s.write(' ]');
    }

    // filter available?
    if (_filters.length > 0) {
      // append ", " ?
      if (_anyClauseAdded) {
        _s.write(", ");
      }
      _anyClauseAdded = true;

      _s.write(' "filter": [ ');
      _s.write(_buildQueryBuildersString(_filters));
      _s.write(' ]');
    }
    _s.write(' } }');

    return _s.toString();
  }

  /// Build the array of query(s) associated with any of the must, must_not, should or filter clauses.
  /// 
  /// [builders] provide the query(s) for the associated clause.
  String _buildQueryBuildersString(List<QueryBuilder> builders) {
    StringBuffer _s = StringBuffer();
    int _i = 0;

    builders.forEach((_builder) {
      if (_i > 0) {
        _s.write(", ");
      }
      _s.write(_builder.buildQueryPartsOnly());
      _i++;
    });
    return _s.toString();
  }

  @override
  String build() {
    StringBuffer _s = StringBuffer();
    String _v;

    _s.write('{ ');

    // _source available?
    if (__source != null) {
      _s.write(buildSourcePartsOnly());
      _s.write(", ");
    }

    // query part
    _s.write('"query": ');
    _s.write(buildQueryPartsOnly());
    
    // final closing quote
    _s.write(" }");

    // remove the last ", " if available
    _v = _s.toString();
    return _v;
  }

}
