import 'package:flutter_test/flutter_test.dart';
import 'package:http_api_demo_app/esQueryBuilder.dart';

void main() {

  // Test on Abstract QueryBuilder and implementation MatchQueryBuilder.
  test("test on Abstract QueryBuilder features + MatchQueryBuilder", () {
    final QueryBuilder _b = MatchQueryBuilder();
    
    // a. TEST on _source

    String _q = _b.sourceInclude([ "age", "gender", "weight" ]).buildSourcePartsOnly();
    expect(_q, '"_source": { "includes": [ "age", "gender", "weight" ] }');

    // empty includes => means everything.
    _q = _b.sourceInclude([]).buildSourcePartsOnly();
    expect(_q, '"_source": { "includes": "*" }');

    _q = _b.sourceInclude(null).buildSourcePartsOnly();
    expect(_q, '"_source": { "includes": "*" }');

    // b. TEST on ???
    

  });

  // Test on MatchQueryBuilder.
  test("test on MatchQueryBuilder", () {
    final _b = MatchQueryBuilder();

    // a. TEST on queryParts

    // String field
    String _q = _b.field("content_italian", "ciao", operator: Operator.and).buildQueryPartsOnly();
    expect(_q, '{ "match": { "content_italian": { "query": "ciao", "operator": "and" } } }');

    // int field
    _q = _b.field("age", 18).buildQueryPartsOnly();
    expect(_q, '{ "match": { "age": { "query": 18, "operator": "or" } } }');

    // float fields
    _q = _b.field("price", 1800.5).buildQueryPartsOnly();
    expect(_q, '{ "match": { "price": { "query": 1800.5, "operator": "or" } } }');

    // b. TEST on build

    _q = _b.field("content_italian", "ciao", operator: Operator.and).build();
    expect(_q, '{ "_source": { "includes": "*" }, "query": { "match": { "content_italian": { "query": "ciao", "operator": "and" } } } }');

    _q = _b.field("day_of_week_i", 5).sourceInclude([ "day_of_week", "category", "taxful_total_price" ]) .build();
    expect(_q, '{ "_source": { "includes": [ "day_of_week", "category", "taxful_total_price" ] }, "query": { "match": { "day_of_week_i": { "query": 5, "operator": "or" } } } }');

  });

  // Test on RangeQueryBuilder.
  test("test on RangeQueryBuilder", () {
    final RangeQueryBuilder _b = RangeQueryBuilder();

    // a. TEST on query parts.

    String _q = _b.field("taxful_total_price", 100).buildQueryPartsOnly();
    expect(_q, '{ "range": { "taxful_total_price": { "gte": 100 } } }');

    _q = _b.field("taxful_total_price", 1000.89, ops: RangeOperator.lt).buildQueryPartsOnly();
    expect(_q, '{ "range": { "taxful_total_price": { "lt": 1000.89 } } }');

    // 2 operator values e.g. lt + gte
    _q = _b.field("taxful_total_price", 1000.89, ops: RangeOperator.lt, value2: 200.1, ops2: RangeOperator.gt).buildQueryPartsOnly();
    expect(_q, '{ "range": { "taxful_total_price": { "lt": 1000.89, "gt": 200.1 } } }');

    // b. TEST on query (all)

    _q = _b.field("taxful_total_price", 100).sourceInclude(["day_of_week", "category"]).build();
    expect(_q, '{ "_source": { "includes": [ "day_of_week", "category" ] }, "query": { "range": { "taxful_total_price": { "gte": 100 } } } }');

  });

  // Test on BoolQueryBuilder.
  test("test on BoolQueryBuilder", () {
    final BoolQueryBuilder _b = BoolQueryBuilder();

    // a. TEST on query parts.

    // only MUSTs (2 elements)
    _b.addMust(MatchQueryBuilder().field("day_of_week_i", 4)).
      addMust(RangeQueryBuilder().field("taxful_total_price", 10, ops: RangeOperator.lte));
    expect(_b.buildQueryPartsOnly(), '{ "bool": {  "must": [ { "match": { "day_of_week_i": { "query": 4, "operator": "or" } } }, { "range": { "taxful_total_price": { "lte": 10 } } } ] } }');

    // only 1 must
    _b.reset();
    _b.addMust(MatchQueryBuilder().field("day_of_week_i", 5));
    expect(_b.buildQueryPartsOnly(), '{ "bool": {  "must": [ { "match": { "day_of_week_i": { "query": 5, "operator": "or" } } } ] } }');

    // every clause with 1 or 2 entries
    _b.reset();
    _b.addMust(MatchQueryBuilder().field("day_of_week_i", 5)).
      addMust(RangeQueryBuilder().field("taxful_total_price", 100.0)).
      addMustNot(MatchQueryBuilder().field("customer_first_name", "Selena")).
      addShould(MatchQueryBuilder().field("customer_first_name", "Marwan")).
      addShould(RangeQueryBuilder().field("day_of_week_i", 6, ops: RangeOperator.gte)).
      addFilter(MatchQueryBuilder().field("manufacturer.keyword", "Angeldale"));
    expect(_b.buildQueryPartsOnly(), '{ "bool": {  "must": [ { "match": { "day_of_week_i": { "query": 5, "operator": "or" } } }, { "range": { "taxful_total_price": { "gte": 100.0 } } } ],  "must_not": [ { "match": { "customer_first_name": { "query": "Selena", "operator": "or" } } } ],  "should": [ { "match": { "customer_first_name": { "query": "Marwan", "operator": "or" } } }, { "range": { "day_of_week_i": { "gte": 6 } } } ],  "filter": [ { "match": { "manufacturer.keyword": { "query": "Angeldale", "operator": "or" } } } ] } }');

    // b. TEST on build all
    _b.reset();
    _b.addMust(MatchQueryBuilder().field("day_of_week_i", 4)).
      addMust(RangeQueryBuilder().field("taxful_total_price", 10, ops: RangeOperator.lte)).
      sourceInclude(["customer_first_name"]);
    expect(_b.build(), '{ "_source": { "includes": [ "customer_first_name" ] }, "query": { "bool": {  "must": [ { "match": { "day_of_week_i": { "query": 4, "operator": "or" } } }, { "range": { "taxful_total_price": { "lte": 10 } } } ] } } }');

    _b.reset();
    _b.addMust(MatchQueryBuilder().field("day_of_week_i", 5));
    expect(_b.build(), '{ "_source": { "includes": "*" }, "query": { "bool": {  "must": [ { "match": { "day_of_week_i": { "query": 5, "operator": "or" } } } ] } } }');

    _b.reset();
    _b.addMust(MatchQueryBuilder().field("day_of_week_i", 5)).
      addMust(RangeQueryBuilder().field("taxful_total_price", 100.0)).
      addMustNot(MatchQueryBuilder().field("customer_first_name", "Selena")).
      addShould(MatchQueryBuilder().field("customer_first_name", "Marwan")).
      addShould(RangeQueryBuilder().field("day_of_week_i", 6, ops: RangeOperator.gte)).
      addFilter(MatchQueryBuilder().field("manufacturer.keyword", "Angeldale"));
    expect(_b.build(), '{ "_source": { "includes": "*" }, "query": { "bool": {  "must": [ { "match": { "day_of_week_i": { "query": 5, "operator": "or" } } }, { "range": { "taxful_total_price": { "gte": 100.0 } } } ],  "must_not": [ { "match": { "customer_first_name": { "query": "Selena", "operator": "or" } } } ],  "should": [ { "match": { "customer_first_name": { "query": "Marwan", "operator": "or" } } }, { "range": { "day_of_week_i": { "gte": 6 } } } ],  "filter": [ { "match": { "manufacturer.keyword": { "query": "Angeldale", "operator": "or" } } } ] } } }');

  });


}
